# Responsive Grid Layout

Responsive Grid Layout using CSS compiled from SCSS. The layout (cards with a featured card) created with the use of the CSS grid and media query for realigning the card components depending upon the current width of the browser.

## Layout

### Standard Size

![WebsiteLayout-1](./html-css-dynamic-grid-layout-layout-1.png)

### Small Size

![WebsiteLayout-2](./html-css-dynamic-grid-layout-layout-2.png)